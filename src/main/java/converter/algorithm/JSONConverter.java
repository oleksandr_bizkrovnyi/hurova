package converter.algorithm;

import com.google.gson.Gson;
import converter.util.Converter;
import model.Action;
import web.FileController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JSONConverter implements Converter<Action> {

    @Override
    public Action toObject(Class<? extends Action> action, String path) {
        try {
            return new Gson().fromJson(readFile(path),action);
        } catch (IOException e) {
            System.out.println("Unable to read file");
            throw new IllegalArgumentException("File Url is broken");
        }
    }

    @Override
    public void fromObject(Action action) {
        System.out.println("Start generating JSON file");
        String result = new Gson().toJson(action);
        String path = System.getProperty("user.home") + "/Desktop/JSON_" + System.currentTimeMillis() + ".txt";
        try {
            Files.write(Paths.get(path), result.getBytes());
            System.out.println("Now you can find your file by this path: " + path);
            new FileController().sendPost(path,"JSON","http://localhost:7414/action/create");
        } catch (IOException e) {
            System.out.println("File saving is impossible");
        }
    }
}
