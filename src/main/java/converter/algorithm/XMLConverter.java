package converter.algorithm;


import converter.util.Converter;
import model.Action;
import web.FileController;

import javax.xml.bind.*;
import java.io.File;

public class XMLConverter implements Converter<Action> {


    @Override
    public Action toObject(Class<? extends Action> claz, String path) {

        try {
            File file = new File(path);
            JAXBContext jaxbContext = JAXBContext.newInstance(claz);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (Action) jaxbUnmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            throw new IllegalArgumentException(e);
        }

    }


    @Override
    public void fromObject(Action action) {
        String path = System.getProperty("user.home") + "/Desktop/XML_" + System.currentTimeMillis() + ".xml";
        File file = new File(path);
        System.out.println("Start generating XML file");
        System.out.println("Now you can find your file by this path: " + path);
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(Action.class);

            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(action, file);
            jaxbMarshaller.marshal(action, System.out);
            new FileController().sendPost(path,"XML","http://localhost:7414/action/create");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
