package converter.algorithm;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import converter.util.Converter;
import model.Action;
import web.FileController;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class CSVConverter implements Converter<Action> {


    @Override
    public Action toObject(Class<? extends Action> claz, String path) {
        try{
            Reader reader = new FileReader(path);
            Iterator<Map<String, String>> iterator = new CsvMapper()
                    .readerFor(Map.class)
                    .with(CsvSchema.emptySchema().withHeader())
                    .readValues(reader);
            Map<String, String> keyVals = new HashMap<>();
            while (iterator.hasNext()) {
                keyVals = iterator.next();
            }
            return  new Action().fromMap(keyVals);
        }catch (IOException  e){
            throw new IllegalArgumentException("Unable to find file", e);
        }
    }

    @Override
    public void fromObject(Action action) {
        try {
            System.out.println("Start generating CSV file");
            String path = System.getProperty("user.home") + "/Desktop/CSV_" + System.currentTimeMillis() + ".csv";
            System.out.println("Now you can find your file by this path: " + path);
            File tempFile = new File(path);
            FileOutputStream tempFileOutputStream = new FileOutputStream(tempFile);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(tempFileOutputStream, 1024);
            OutputStreamWriter writerOutputStream = new OutputStreamWriter(bufferedOutputStream, StandardCharsets.UTF_8);
            List<Map<String,String>> mapList = new ArrayList<>();
            mapList.add(action.toMap());
            csvWriter(mapList,writerOutputStream);
            new FileController().sendPost(path,"CSV","http://localhost:7414/action/create");
        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }

    }

    private static void csvWriter(List<Map<String, String>> listOfMap, Writer writer) throws IOException {
        CsvSchema schema = null;
        CsvSchema.Builder schemaBuilder = CsvSchema.builder();
        if (listOfMap != null && !listOfMap.isEmpty()) {
            for (String col : listOfMap.get(0).keySet()) {
                schemaBuilder.addColumn(col);
            }
            schema = schemaBuilder.build().withLineSeparator("\r").withHeader();
        }
        CsvMapper mapper = new CsvMapper();
        mapper.writer(schema).writeValues(writer).writeAll(listOfMap);
        writer.flush();
    }
}
