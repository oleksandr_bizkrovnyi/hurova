package converter.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public interface Converter<T> {

    T toObject(Class<? extends T> claz, String path);

    void fromObject(T action);

    default String readFile(String path) throws IOException {
        StringBuilder result = new StringBuilder();
        Files.lines(Paths.get(path)).forEach(result::append);
        return result.toString();
    }
}
