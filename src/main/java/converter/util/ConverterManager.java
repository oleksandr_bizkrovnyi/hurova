package converter.util;

import converter.algorithm.CSVConverter;
import converter.algorithm.JSONConverter;
import converter.algorithm.XMLConverter;
import model.Action;

public class ConverterManager {

    public static Converter<Action> getConverter(Class<? extends Converter> convType) {
        if (convType.equals(CSVConverter.class)) {
            return new CSVConverter();
        } else if (convType.equals(JSONConverter.class)) {
            return new JSONConverter();
        } else if (convType.equals(XMLConverter.class)) {
            return new XMLConverter();
        }
        throw new IllegalArgumentException("Converter type is not found");
    }
}
