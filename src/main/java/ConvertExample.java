import converter.algorithm.CSVConverter;
import converter.algorithm.JSONConverter;
import converter.algorithm.XMLConverter;
import converter.util.Converter;
import converter.util.ConverterManager;
import model.Action;
import model.TestAction;

import java.util.Scanner;

public class ConvertExample {

    public static void main(String[] args) {

        System.out.println("Choose one of the TypeConverter: XML, JSON or CSV");
        Scanner scanner = new Scanner(System.in);
        Action action = TestAction.createTestAction();
        Converter<Action> converter;
        if (scanner.hasNext()) {
            switch (scanner.next()) {
                case "XML":
                    converter = ConverterManager.getConverter(XMLConverter.class);
                    converter.fromObject(action);
                    break;
                case "JSON":
                    converter = ConverterManager.getConverter(JSONConverter.class);
                    converter.fromObject(action);
                    break;
                case "CSV":
                    converter = ConverterManager.getConverter(CSVConverter.class);
                    converter.fromObject(action);
            }
        }
    }


}
