package web;

import model.Action;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import sun.net.www.http.HttpClient;


import javax.xml.crypto.OctetStreamData;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FileController {

    public void sendPost(Action action, String url) {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost(url);
            httppost.setEntity(new UrlEncodedFormEntity(createBody(action), "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                System.out.println(entity);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }


    public void sendPost(String pathFileAction, String type, String url) {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost(url);
            httppost.setEntity(createBody(pathFileAction, type));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                System.out.println(entity);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }


    private List<NameValuePair> createBody(Action action) {
        List<NameValuePair> params = new ArrayList<>(2);
        action.toMap().forEach((key, value) -> params.add(new BasicNameValuePair(key, value)));
        return params;
    }

    private HttpEntity createBody(String path, String type) {
        File initialFile = new File(path);
        try {
            InputStream targetStream = FileUtils.openInputStream(initialFile);
            byte[] data = IOUtils.toByteArray(targetStream);
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.addTextBody("type", type);
            builder.addBinaryBody("data", data, ContentType.APPLICATION_OCTET_STREAM, "asdasd.xx");
            return builder.build();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Body creation is impossible", e);
        }

    }

}
