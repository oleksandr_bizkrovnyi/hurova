package model;

import java.util.HashMap;
import java.util.Map;

public interface MapConverter<T> {
    Map toMap();

    T fromMap(Map<String,String> paramValues);
}
