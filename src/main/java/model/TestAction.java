package model;


import java.sql.Date;
import java.time.Instant;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class TestAction extends Action {

    public static Action createTestAction() {
        Action action = new TestAction();
        action.setId(UUID.randomUUID().toString());
        action.setDeviceId("123-123-123");
        action.setOs("macOS");
        action.setPlayTime(Date.from(Instant.now()).toString());
        action.setUserId(UUID.randomUUID().toString());
        action.setUserLocale(Locale.getDefault().toString());
        action.setParameters(getParamMap());
        return action;
    }


    private static Map<String, String> getParamMap() {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("lifeCount", "2");
        paramMap.put("dogLifeCount", "3");
        paramMap.put("bonusCoin", "345");
        return paramMap;
    }
}
