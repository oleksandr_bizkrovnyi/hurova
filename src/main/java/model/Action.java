package model;


import adapter.MapAdapter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.ToString;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@XmlRootElement
@ToString
@JsonPropertyOrder({"id", "userId", "deviceId", "userLocale", "os", "playTime", "parameters"})
public class Action implements MapConverter<Action> {

    private String id;

    private String userId;

    private String deviceId;

    private String userLocale;

    private String os;

    private String playTime;

    private Map<String, String> parameters;

    @XmlElement
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlElement
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @XmlElement
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @XmlElement
    public String getUserLocale() {
        return userLocale;
    }

    public void setUserLocale(String userLocale) {
        this.userLocale = userLocale;
    }

    @XmlElement
    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    @XmlElement
    public String getPlayTime() {
        return playTime;
    }

    public void setPlayTime(String playTime) {
        this.playTime = playTime;
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public Map<String,String> toMap() {
        Map<String, String> fieldMap = new HashMap<>();
        fieldMap.put("id", id);
        fieldMap.put("userId", userId);
        fieldMap.put("deviceId", deviceId);
        fieldMap.put("userLocale", userLocale);
        fieldMap.put("os", os);
        fieldMap.put("playTime", playTime);
        for (Map.Entry<String, String> item : parameters.entrySet()) {
            fieldMap.put(item.getKey(), item.getValue());
        }
        return fieldMap;
    }


    @Override
    public Action fromMap(Map<String, String> fieldValues) {
        Map<String, String> parameters = new HashMap<>();
        fieldValues.forEach((key, value) -> {
            switch (key) {
                case "id":
                    id = value;
                    break;
                case "userId":
                    userId = value;
                    break;
                case "deviceId":
                    deviceId = value;
                    break;
                case "userLocale":
                    userLocale = value;
                    break;
                case "os":
                    os = value;
                    break;
                case "playTime":
                    playTime = value;
                    break;
                default: {
                    parameters.put(key, value);
                }
            }
        });
        this.parameters = parameters;
        return this;
    }
}
